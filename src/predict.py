"""

Author: Andraž Pelicon

"""

from src.data_transformation import prepare_data_for_prediction

import torch
from transformers import BertTokenizer, BertConfig, BertForSequenceClassification
from src.MLBertModelForClassification import BertClassificationTraining


def predict(data, max_len=512, batch_size=32):
    """Runs the data thorugh the news sentiment model and returns model's predictions.
    The predictions are encoded as follows: 0 - negative; 1 - neutral; 2 - positive
    :param data: (list) list of input text documents
    :param max_len: (int) maximum length of input sequence
    :param batch_size: (int) batch size
    :return (list) model predictions
    """
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    tokenizer = BertTokenizer.from_pretrained('bert-base-multilingual-cased', do_lower_case=False)

    config = BertConfig.from_pretrained('model/config.json')
    model = BertForSequenceClassification.from_pretrained('model/pytorch_model.bin',
                                                          config=config)
    bert_trainer = BertClassificationTraining(model, device)

    dataloader = prepare_data_for_prediction(data, tokenizer, max_len, batch_size)
    predictions = bert_trainer.predict(dataloader)
    return predictions

